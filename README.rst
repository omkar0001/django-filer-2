==============
django-filer-2
==============

This is an extension of django-filer https://github.com/stefanfoulis/django-filer.git

A file management application for django that makes handling of files and images a breeze.

.. figure:: docs/_static/directory_view_1_screenshot.png
   :scale: 50 %
   :alt: file browser

Filer picker widget: |file-picker-widget-screeshot|

.. |file-picker-widget-screeshot| image:: docs/_static/default_admin_file_widget.png


Documentation: http://django-filer.readthedocs.org/en/latest/index.html


Dependencies
------------

* `Django`_ >= 1.4
* `django-mptt`_ >=0.5.1
* `easy_thumbnails`_ >= 1.0
* `django-polymorphic`_ >= 0.2
* `Pillow`_ 2.3.0 (with JPEG and ZLIB support, `PIL`_ 1.1.7 is supported but not recommended)

``django.contrib.staticfiles`` is required.

**Django >= 1.6** is supported together with `django-polymorphic`_ >= 0.5.4

Configuration
-------------

Add ``"filer"``, ``"mptt"`` and ``"easy_thumbnails"`` to your project's ``INSTALLED_APPS`` setting and run ``syncdb``
(and ``migrate`` if you're using South).

Add 

THUMBNAIL_PROCESSORS = (
    'easy_thumbnails.processors.colorspace',
    'filer.thumbnail_processors.scale_and_crop_with_area_interest',
    'easy_thumbnails.processors.filters',
)

in settings.py

See the docs for advanced configuration:

  * `subject location docs`_
  * `permission docs`_ (experimental)
  * `secure file downloads docs`_ (experimental)


Testsuite
---------

The easiest way to run the testsuite is to checkout the code, make sure you have ``PIL`` installed, and run::

    python setup.py test


