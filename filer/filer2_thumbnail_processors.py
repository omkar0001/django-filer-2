#-*- coding: utf-8 -*-
import re
try:
    from PIL import Image
    from PIL import ImageDraw
except ImportError:
    try:
        import Image
        import ImageDraw
    except ImportError:
        raise ImportError("The Python Imaging Library was not found.")
from easy_thumbnails import processors
from filer.settings import FILER_SUBJECT_LOCATION_IMAGE_DEBUG

RE_SUBJECT_LOCATION = re.compile(r'^(\d+),(\d+)$')


def normalize_area_interest_coords(area_interest_coords):
    """ Given a string of top left(x,y), bottom(x,y) of a rectangular box in a string.
        It will return the list of coordinates. 

        Lets say the string is 23,10,43,20. It will return [23,10,43,20]
    """
    if area_interest_coords:
        list_coords = area_interest_coords.split(',')
        return list_coords
    else:
        return False


def scale_and_crop_with_area_interest(im, size, area_interest_coords, 
                                        crop=False, upscale=False, **kwargs):
    """
    This will first crop the the image with area of interest and then upscale or
    downscale.

    Note:- Better not to use any crop processor before this.
    """    
    area_interest_coords = normalize_area_interest_coords(area_interest_coords)
    if not (area_interest_coords and crop):
        return processors.scale_and_crop(im, size, crop=crop, upscale=upscale, **kwargs)
    
    # Cropping by the area of interest. using the top left and bottom right coordinates.
    crop_box = ((
                int(float(area_interest_coords[0])), int(float(area_interest_coords[1])), 
                int(float(area_interest_coords[2])), int(float(area_interest_coords[3]))
                ))
    im = im.crop(crop_box)
    # --snip-- this is a copy and paste of the first few
    #          lines of ``scale_and_crop``

    source_x, source_y = [float(v) for v in im.size]
    target_x, target_y = [float(v) for v in size]

    if crop or not target_x or not target_y:
        scale = max(target_x / source_x, target_y / source_y)
    else:
        scale = min(target_x / source_x, target_y / source_y)



    #Resizing the cropped images.
    if scale < 1.0 or (scale > 1.0 and upscale):
        im = im.resize((int(target_x), int(target_y)),
                       resample=Image.ANTIALIAS)
    return im