#-*- coding: utf-8 -*-
import logging
import json

from filer.models.imagemodels import Image
from django.db import models
from django.utils.translation import ugettext_lazy as _
from filer import settings as filer_settings

logger = logging.getLogger("filer")


def coords2csv(coords):
    return ','.join(map(str, coords))


def calculate_height(aspect_ratio, width):
    """ Returns the height when the width and the aspect_ratio are provided

        :param aspect_ratio: String of the form 'width-height' eg: 16-9
        :param width: Number

        :return: Integer
    """
    ar_width, ar_height = map(float, aspect_ratio.split('-'))
    height = int((ar_height * int(width)) / ar_width)

    return height


class Picture(Image):
    area_interest_coords = models.TextField(_('area_interest_coords'), null=True, blank=True, \
                                            default=None)
    area_interest_onfly_crop_coords = models.TextField(_('area_interest_coords'), null=True, blank=True, \
                                                       default=None)

    @property
    def icons(self):
        required_thumbnails = {}

        area_interest_coords_json = None
        if self.area_interest_coords is not None:
            area_interest_coords_json = json.loads(self.area_interest_coords)

        # Goes through each aspect ratio and also the sizes under it.
        # Scales the images to those sizes.
        for aspect_ratio, sizes in filer_settings.FILER2_ADMIN_ICON_SIZES.iteritems():

            try:
                area_interest_coords = area_interest_coords_json[aspect_ratio]
            except KeyError:
                area_interest_coords = None 

            if area_interest_coords is None:
                continue

            for width in sizes:
                width = int(width)

                height = calculate_height(aspect_ratio, width)

                # No point in upscaling an image and providing a low res pixellated version
                try:
                    x1, y1, x2, y2 = area_interest_coords
                except:
                    continue

                width = int(min((x2 - x1) * self.width, int(width)))
                height = int(min((y2 - y1) * self.height, int(height)))

                ar_code = filer_settings.FILER2_ASPECT_RATIOS.get(aspect_ratio)
                key = '{0}-{1}'.format(ar_code, width)

                required_thumbnails[key] = {
                        'size': (width, height),
                        'crop': True,
                        'upscale': True,
                        'area_interest_coords': coords2csv(self.denormalize_coords(area_interest_coords)),
                        }

        generated_thumbnails = self._generate_thumbnails(required_thumbnails)

        # Since the squared thumbnails are now keyed as 'sq-w,h' we need the
        # generated thumbnails to have an additional set of thumbnails
        # refering to the same square thumbnails with key as the width (= height)
        for key in generated_thumbnails.keys():
            if key.startswith('sq-'):
                generated_thumbnails[key.split('-')[1]] = generated_thumbnails[key]

        return generated_thumbnails

    @property
    def icons_cropped_onfly(self):
        if self.area_interest_onfly_crop_coords is None:
            return None

        onthefly_areas_coords = json.loads(self.area_interest_onfly_crop_coords)

        thumbnails_params = dict(
                (area_interest_coords, {
                    'size': (self.width, self.height),
                    'crop': True,
                    'upscale': True,
                    'area_interest_coords': self.denormalize_coords(area_interest_coords)
                    })
                for area_interest_coords in onthefly_areas_coords)

        return self.generate_thumbnails(thumbnails_params)

    def denormalize_coords(self, coords):
        """ This function takes top left and bottom right coordinates as a CSV
            and scales them to absolute pixel coordinates in the image and returns
            them as a top-left and bottom-right coordinates CSVs

            The individual coordinates in the final csv are in integer values
        """
        try:
            x1, y1, x2, y2 = coords
        except(ValueError):
            return [0, 0, self.width, self.height]

        x1, y1 = int(float(x1) * self.width), int(float(y1) * self.height)
        x2, y2 = int(float(x2) * self.width), int(float(y2) * self.height)

        return [x1, y1, x2, y2]

    class Meta:
        app_label = 'filer'
        verbose_name = _('picture')
        verbose_name_plural = _('pictures')
