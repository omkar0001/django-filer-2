#-*- coding: utf-8 -*-
import json

from PIL import Image as PILImage

from django import forms
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse
from django.utils import simplejson

from filer import settings as filer_settings
from filer.admin.fileadmin import FileAdmin
from filer.models import Picture


def normalize_coords2(coords, height, width):
    """ Returns the normalized values of the absoluted coordinates of the crop_picture
        area in the image. By normalized coordinates the values of x, y are between
        0.0 and 1.0

        :param coords: list of coordinates - x, y (top-left) & (bottom-right)
        :param height: image height
        :param width: image width

        :return: list of normalized coordinates
    """
    try:
        x1, y1, x2, y2 = coords
    except:
        return None

    x1, y1 = x1 / float(width), y1 / float(height)
    x2, y2 = x2 / float(width), y2 / float(height)
    
    return [x1, y1, x2, y2]


def thumbnail_sort_key(k):
    """ The thumbnail key is of the form '<AR>-<W>'
        AR: Aspect ratio
        W: Width

        The square thumbnails are of the form '<W>'

        This function sorts the thumbnail keys by aspect_ratio and then by the
        width of the thumbnail
    """
    try:
        # Sort key for square thumbnails
        return (int(k), None)
    except:
        # Sort key tuple (aspect_ratio, width) for other thumbnails
        ar, size = k.split('-')
        return (ar, int(size))


class PictureAdminForm(forms.ModelForm):
    def sidebar_image_ratio(self):
        if self.instance:
            # this is very important. It forces the value to be returned as a
            # string and always with a "." as seperator. If the conversion
            # from float to string is done in the template, the locale will
            # be used and in some cases there would be a "," instead of ".".
            # javascript would parse that to an integer.
            return  "%.6F" % self.instance.sidebar_image_ratio()
        else:
            return ''

    class Meta:
        model = Picture

    class Media:
        css = {
            #'all': (settings.MEDIA_URL + 'filer/css/focal_point.css',)
        }


class PictureAdmin(FileAdmin):
    form = PictureAdminForm

    def change_view(self, request, object_id, form_url='', extra_context=None):
        return super(PictureAdmin, self).change_view(request, object_id, form_url,
                                                     extra_context=extra_context)
   
    def crop_picture(self, picture, areas_of_interest):
        """ Crops the picture according to the coordinates and scale.

            :param picture: Picture object
            :param areas_of_interest: Coordinates string keyed for
                each aspect ratio. These are the coordinates of the crop
                area of the image that appears on UI.

            .. note::
                Format for areas_of_interest is [x1, y1, x2, y2]
        """
        picture.area_interest_coords = json.dumps(areas_of_interest)
        picture.save()

        return picture


    @csrf_exempt
    def crop_image(self, request):
        """ Crops an given image, with the left and top coordinates given, that
            are retrieved from the request.

            - First the coordinates for each aspect ratio are normalized.
            - Then they are cropped as per the aspect ratio and resolutions
              required and stored in the 'icons' attribute
            - Then each of the thumbnail generated for each aspect ratio and
              each resolution are sorted in order to be sent to the UI for
              preview

        """
        mimetype = 'application/json' if request.is_ajax() else 'text/html'

        pk_id = int(request.POST.get('fileId'))
        height = int(request.POST.get('height'))
        width = int(request.POST.get('width'))
        picture = Picture.objects.get(pk=pk_id)

        area_of_interest = {}

        # Makes sure that no bespoke aspect ratios are not created
        for aspect_ratio in filer_settings.FILER2_ADMIN_ICON_SIZES.keys():
            try:
                area_coords = map(float, request.POST.get(aspect_ratio).split(','))
                area_of_interest[aspect_ratio] = normalize_coords2(area_coords, height, width)
            except:
                pass

        self.crop_picture(picture, area_of_interest)
        
        # Needed to make sure the 'icons' property method is invoked only once
        icons = picture.icons

        # Getting all the generated thumbnails in sorted order of the aspect ratio
        # All thumbnails of same aspect ratio are grouped and ordered from small
        # to big
        generated_thumbnails = {}

        sorted_icons = sorted(icons.items(), key=lambda t: thumbnail_sort_key(t[0]))
        for key, thumbnail in sorted_icons:
            try:
                ar, size = key.split('-')
            except:
                continue

            if ar not in generated_thumbnails:
                generated_thumbnails[ar] = []

            generated_thumbnails[ar].append(thumbnail)

        json_response = {
            'thumbnail': icons['32'],
            'generated_thumbnails': generated_thumbnails,
        }

        return HttpResponse(simplejson.dumps(json_response), mimetype=mimetype)

    @csrf_exempt
    def change_picture_on_fly(self, request, picture_id):
        picture = Picture.objects.get(pk=picture_id)
        crop_coords = request.GET.get('crop_coords')
        max_width = request.GET.get('max_width')

        try:
            picture_coords_onfly_json = json.loads(picture.area_interest_onfly_crop_coords)
        except(TypeError):
            picture_coords_onfly_json = {}

        picture_coords_onfly_json[crop_coords] = crop_coords
        picture.area_interest_onfly_crop_coords = json.dumps(picture_coords_onfly_json)
        picture.save()

        return render_to_response(
                'admin/filer/picture/picture_on_fly_edit.html', {
                    'picture_id': picture_id,
                    'picture_object': picture,
                    'cropped_picture_url': picture.icons_cropped_onfly[crop_coords],
                    'max_width': max_width
                },
                context_instance=RequestContext(request))

    def get_urls(self):
        try:
            # django >=1.4
            from django.conf.urls import patterns, url
        except ImportError:
            # django <1.4
            from django.conf.urls.defaults import patterns, url

        urls = super(PictureAdmin, self).get_urls()
        from filer import views
        url_patterns = patterns('',
            url(r'^content-images/(?P<picture_id>[\w-]+)/$',
                self.admin_site.admin_view(self.change_picture_on_fly),
                name='filer-change_picture_on_fly'),

            # upload does it's own permission stuff (because of the stupid flash
            # missing cookie stuff)
            url(r'^operations/crop_image/$', self.crop_image, name='filer-crop_image')
        )
        url_patterns.extend(urls)
        return url_patterns
    

PictureAdmin.fieldsets = PictureAdmin.build_fieldsets(
    extra_main_fields=('default_alt_text', 'default_caption',),
    extra_fieldsets=(
        ('Subject Location', {
            'fields': ('subject_location',),
            'classes': ('collapse',),
        }),
    )
)
