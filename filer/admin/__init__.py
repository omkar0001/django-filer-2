#-*- coding: utf-8 -*-
from django.contrib import admin
from filer.admin.clipboardadmin import ClipboardAdmin
from filer.admin.fileadmin import FileAdmin
from filer.admin.folderadmin import FolderAdmin
from filer.admin.extendedfolderadmin import ExtendedFolderAdmin
from filer.admin.imageadmin import ImageAdmin
from filer.admin.pictureadmin import PictureAdmin
from filer.admin.permissionadmin import PermissionAdmin
from filer.models import FolderPermission, Folder, File, Clipboard, Image, Picture


admin.site.register(Folder, FolderAdmin)
#admin.site.register(Folder, ExtendedFolderAdmin)
admin.site.register(File, FileAdmin)
admin.site.register(Clipboard, ClipboardAdmin)
admin.site.register(Image, ImageAdmin)
admin.site.register(FolderPermission, PermissionAdmin)
admin.site.register(Picture, PictureAdmin)
