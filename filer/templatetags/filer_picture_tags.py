from django.template import Library
import json

register = Library()


@register.filter
def get(mapping, key):
    """ Tag for getting the value of a key in the template itself 

        :usage: {{  mapping|get:key }}
    """
    try:
        if type(mapping) in (str, unicode):
            return json.loads(mapping).get(key, '')
        else:
            mapping.get(key, '')
    except:
        return ''
